import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';

import {AppConfig} from './../app-config.provider';

@Injectable()
export class AuthService {
    
  private token: any;
  private idUser: any;
  constructor(public http: Http, public storage: Storage, public appConfig: AppConfig) {
  }

  login(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(this.appConfig.apiEndpoint + 'auth/login', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          let data = res.json();
          this.storage.set('token', data.token);
          this.idUser = data.user._id;
          this.storage.set('id', data.user_id);
          this.storage.set('profile', data.user);
          resolve(data.user);
        }, (err) => {
          reject(err);
        });
    });
  }

  getUserLogged() {
    return new Promise((resolve, reject) => {
      this.storage.get('profile')
        .then((value) => {
            resolve(value);
        })
        .catch(function (err) {
            reject(err);
        });
    });
  }

  getUser() {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
        this.token = value;
				let headers = new Headers();
				headers.append('Authorization', this.token);
				this.http.get(this.appConfig.apiEndpoint + 'user/' + this.idUser, {headers: headers})
						.map(res => res.json())
						.subscribe(data => {
								resolve(data);
						}, (err) => {
								console.log("Falha ao conectar com o servidor.");
								reject(err);
						});
			});
    });
  }

  updateUser(data: object) {
		console.log('idUser:',this.idUser)
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;
				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);
        this.http.put(this.appConfig.apiEndpoint + 'user/' + this.idUser, JSON.stringify(data), {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);
					}, (err) => {
						reject(err);
					});
			});
		});
  }
  
	logout(): void {
		this.storage.remove('profile');
  };
  
  chat(credentials){    
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

        this.http.post(this.appConfig.apiEndpoint + 'chat', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          let data = res.json();
          this.storage.set('chat', data.chat);
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  };
}
