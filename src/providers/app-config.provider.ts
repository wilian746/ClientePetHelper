export class AppConfig {
    private url: string;
    public apiEndpoint: string;
    public socketEndpoint: string;
    public wit : string;

    constructor() {
       
       this.url = 'http://18.221.210.161:9000/';
       //this.url = 'http://localhost:9000/';
        
        this.apiEndpoint = this.url + 'api/v1/';
        this.socketEndpoint = this.url;
    }
}