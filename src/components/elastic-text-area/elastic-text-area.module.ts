import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElasticTextAreaComponent } from './elastic-text-area';

@NgModule({
  declarations: [
    ElasticTextAreaComponent,
  ],
  imports: [
    IonicPageModule.forChild(ElasticTextAreaComponent),
  ],
  exports: [
    ElasticTextAreaComponent
  ]
})
export class ElasticTextAreaComponentModule {}
