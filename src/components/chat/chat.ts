import { Component } from '@angular/core';
@Component({
  selector: 'chat',
  inputs: ['msg: message'],
  templateUrl: 'chat.html'
})
export class ChatComponent {
  public msg: any;
}
