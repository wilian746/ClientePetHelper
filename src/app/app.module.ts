import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppConfig } from "../providers/app-config.provider";
import { AuthService } from "../providers/auth/authService";
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { ClientService } from "../providers/client/clientService";
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { SettingsPage } from '../pages/settings/settings';
import { Geolocation } from '@ionic-native/geolocation';
import { LongPressModule } from 'ionic-long-press';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '89ea1d54'
  }
};
@NgModule({
  declarations: [
    MyApp,
    SettingsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings),
    LongPressModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppConfig,
    AuthService,
    ClientService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    // FileUploadOptions,
    FileTransfer,
    FileTransferObject,
    File,
    Camera,
    Geolocation
    //ServicesModule
  ]
})
export class AppModule {}