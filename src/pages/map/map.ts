import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, IonicPage } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapRef: ElementRef;
  //Marcadores no mapa
  public marker: any;

  constructor(public navCtrl: NavController, public geolocation: Geolocation, public platform: Platform) {
  }

  getUsuarioLatLng(){
    
  }
  //Latitude e Longitute de locais
  public locations = [
    ['PetZoo',-18.582561, -46.518970],
    ['PetMania',-18.585497, -46.511188],
    ['PetZooFilial',-18.586545, -46.510272],
    ['Boi&Cão', -18.587046, -46.511315],
    ['Boi&Cão&Gato',-18.590901, -46.517939],
    ['KãoKiLate',-18.593784, -46.517396],
    ['Zoomed',-18.594306, -46.517335],
    ['Clinicão',-18.601406, -46.516054],
    ['FormulaAnimal',-18.590788, -46.519742],
    ['usuario',-18.574172, -46.513443]
    
  ];
  ionViewDidLoad(){
    this.getUsuarioLatLng()
    this.showMap();
  }
  
  showMap(){
    //posição da pessoa
    var latPessoa: any;
    var lngPessoa: any;

    this.platform.ready().then(() =>{
      this.geolocation.getCurrentPosition().then(resp =>{
      latPessoa = resp.coords.latitude;
      lngPessoa = resp.coords.longitude;
    //opções de mapa "frescuras"
    const options = {
      center: new google.maps.LatLng(latPessoa,lngPessoa),
      animation: google.maps.Animation.DROP,
      zoom: 15,
      mapTypeId: 'terrain'
    }
    //Onde Chama o mapa
    const map = new google.maps.Map(this.mapRef.nativeElement , options);
    //adicionando Marcador no mapa
    this.addMarker(this.marker, map)
    }).catch(() => {
      console.log('Erro ao conseguir a localição da pessoa')
    })
  })
  }
  addMarker(position,map){
    for (let i = 0; i < this.locations.length; i++) {  
        this.marker = new google.maps.Marker({
        position: new google.maps.LatLng(this.locations[i][1], this.locations[i][2]),
        map: map
      });
    }
  }
}
