import {Component} from '@angular/core';
import {IonicPage, LoadingController, MenuController, NavController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {AuthService} from "../../providers/auth/authService";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private formLogin: FormGroup;
  private loading: any;
  public error;
  public showPass = false;
  public type= "password";
  
  constructor(public fb: FormBuilder,
              public menuCtrl: MenuController,
              public authService: AuthService,
              public loadingCtrl: LoadingController,
              public navCtrl: NavController) {
    menuCtrl.enable(false);

    let emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    this.formLogin = fb.group({
      email: [null, Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
			password: [null, [Validators.required, Validators.minLength(6)]]
    })
  }

  login() {
    this.showLoader();

    this.authService.login(this.formLogin.value).then((result) => {
      this.loading.dismiss();
        this.navCtrl.setRoot('ChatPage');
    }, (err) => {
      console.log(err)
      this.error = err;
      this.loading.dismiss();
    });

  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Verificando...'
    });

    this.loading.present();
  }
  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass){
        this.type = "text";
    }
    else {
        this.type = "password";
    }
}
}







