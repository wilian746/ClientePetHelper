import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPage } from './chat';
import { ChatComponentModule } from '../../components/chat/chat.module';
import { ElasticTextAreaComponentModule } from './../../components/elastic-text-area/elastic-text-area.module';

@NgModule({
  declarations: [
    ChatPage,
  ],
  imports: [
    ChatComponentModule,
    ElasticTextAreaComponentModule,
    IonicPageModule.forChild(ChatPage),
  ],
})
export class ChatPageModule {}
