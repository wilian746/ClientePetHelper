import { Component, NgZone, ViewChild } from '@angular/core';
import {
  IonicPage, LoadingController, MenuController, NavController, Content,
  PopoverController, ViewController, NavParams, ToastController, AlertController
} from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from "../../providers/auth/authService";
import { SettingsPage } from '../settings/settings';
import * as moment from 'moment';
import {Storage} from '@ionic/storage';
import { ClientService } from '../../providers/client/clientService';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',

})
export class ChatPage {
  @ViewChild(Content) content: Content;
  paramsUser: any;
  userLogged: any;
  messages: any[] = [];
  showTyping: boolean = false;
  usuarios: any[] = [];
  Users: any;
  private formChat: FormGroup;
  public nomeDoUsuario: any;
  public error;
  public showPass = false;
  public type = "password";
  public data: any;
  constructor(public fb: FormBuilder,
    public menuCtrl: MenuController,
    public authService: AuthService,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public viewCtrl: ViewController,
    public storage : Storage,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public clientService: ClientService,
    private _zone: NgZone,
    private alertCtrl: AlertController,
  ) {
    moment.locale('pt-BR');
    menuCtrl.enable(false);
    this.formChat = fb.group({
      messages: ""
    })
    this.userLogged = this.navParams.data.userLogged
  }
  // Ação que acontece quando a página está prestes a ser exibida
  ionViewWillEnter() {
    this.getUser();
  }

  ionViewDidLoad() {
    let mensagem = 'Bem Vindo ao Pet Helper.\nComo podemos te ajudar ?';
    
    setTimeout(() => {
      this.addMessage(primeiraMensagem);
      this.showTyping = false;
    }, 3000)

    this.showTyping = true;

    let primeiraMensagem = {
      message: mensagem,
      isMe: false,
      date: new Date()
    }
  }

  public addMessage(msg) {
    this._zone.run(() => {
      this.messages.push(msg);
      setTimeout(() => {
        this.content.scrollToBottom(300);
      });
    });
  }
  public addMessageInicio(msg) {
    this._zone.run(() => {
      this.messages.push(msg);
      setTimeout(() => {
        this.content.scrollToBottom(300);
      });
    });
  }

  public mensagem;

  public guardaMensagem = new Array;
  public guardaMensagemChat = new Array;
  public contador: number = 0;

  alertMap(){
    this.navCtrl.push('MapPage');
  }

  mandaMensagem(){
    let guardaMsg = {
      message: this.mensagem.messages,
      isMe: true,
      date: new Date()
    }
    console.log('Contador: \n', this.contador)
    console.log('Mensagem concatenada:\n', this.guardaMensagem)

    this.addMessage(guardaMsg);

    this.formChat.reset();

    this.showTyping = true;
    this.authService.chat([this.guardaMensagem]).then((result) => {
      console.log('Resultado:\n', result);
      if(this.contador < 1){
        this.guardaMensagem = ['']
        this.contador = 0
      }
      let msgBot = {
        message: result,
        isMe: false,
        date: new Date()
      }
      setTimeout(() => {
        this.addMessage(msgBot);
        this.showTyping = false;
      }, 1000)
    }, (err) => {
      console.log(err)
      this.error = err;
    });
  }

  chat() {
    this.contador += + 1;
    this.mensagem = this.formChat.value;
    this.guardaMensagem += this.mensagem.messages;

    if(this.contador > 4){
      this.guardaMensagemChat = [''];
      this.guardaMensagem = ['Vamos voltar ao inicio'];
      this.contador = 0
      let alert = this.alertCtrl.create({
        title: 'Você deseja ver as clínicas proximas?',
        message: 'O PetHelper acabou de achar uma solução para seu problema, você deseja visualizar as clínicas mais próximas?',
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.navCtrl.push('MapPage');
            }
          },
          {
            text: 'Não',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked'); 
            }
          }
        ]
      });
      alert.present();
      this.mandaMensagem();
    }
    if(this.contador > 1 && this.contador < 4){
      this.guardaMensagemChat += this.mensagem.messages;
      console.log('Menssagem Escondida até dar 4: \n', this.guardaMensagemChat)
      this.guardaMensagem = ['Não sei'];
      this.mandaMensagem();
    }
    if(this.contador == 1){
      this.guardaMensagemChat += this.mensagem.messages;
      console.log('Menssagem Escondida até dar 4: \n', this.guardaMensagemChat)
      this.guardaMensagem = this.guardaMensagemChat;
      this.mandaMensagem();
    }
    if(this.contador == 4){
      this.guardaMensagemChat += this.mensagem.messages;
      console.log('Menssagem Escondida DEU 4: \n', this.guardaMensagemChat)
      this.guardaMensagem = this.guardaMensagemChat;
      this.mandaMensagem();
    }
  }
  
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(SettingsPage);
    popover.present({
      ev: myEvent
    });
  }

  public dismiss() {
    this.viewCtrl.dismiss({ messages: this.messages });
  }
  
  getUser(){
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
  });
  loader.present();

  this.authService.getUser()
      .then(res => {
          this.data = res;
          this.nomeDoUsuario = this.data.name;
          loader.dismissAll();

      }, err => {
          this.showToast("Tivemos problemas ao comunicar com o servidor! Faça Login novamente!", 5000);
          loader.dismissAll();
      })
      .catch(err => {
          let body = JSON.parse(err._body);
          let message = body.message;
          this.showToast(message, 3000);
          loader.dismissAll();
      });
  }

  // Método para chamar o Toast
  showToast(message: string, duration?: number) {
    let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        showCloseButton: true,
        closeButtonText: "Ok"
    });
    toast.present();
  }
}
