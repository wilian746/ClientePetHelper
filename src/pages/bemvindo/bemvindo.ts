import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-bemvindo',
  templateUrl: 'bemvindo.html',
})
export class BemvindoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BemvindoPage');
  }

  entrar(){ //Função para navegar para a página de Login
    this.navCtrl.push('LoginPage'); //Puxa a página de Login
  }

  cadastrar(){ //Função para navegar para a página de cadastro
    this.navCtrl.push('CadastroPage'); //Puxa a página de cadastro
  }

}
