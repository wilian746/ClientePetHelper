import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {AuthService} from "../../providers/auth/authService";
/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  template: `
  <ion-list>
    <button ion-item (click)="alterarConta()">
      <div class="meio">ALTERAR CONTA</div>
    </button>
    <ion-buttons end>
    <button ion-item (click)="logout()"> 
      <div class="meio">SAIR</div>
    </button>
    </ion-buttons>
  </ion-list>
`

})
export class SettingsPage {

  constructor(
    public authService: AuthService,
    public navCtrl: NavController, 
    public viewCtrl : ViewController,
    public navParams: NavParams,
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  alterarConta() {
    this.navCtrl.push('AlterarContaPage');
  }
  logout(){
    this.authService.logout();
    this.viewCtrl.dismiss();
    this.navCtrl.setRoot('BemvindoPage');
  }

}
