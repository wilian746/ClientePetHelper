import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { LoadingController, ToastController, NavParams, NavController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/authService';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ClientService } from '../../providers/client/clientService';

@IonicPage()
@Component({
  selector: 'page-alterar-conta',
  templateUrl: 'alterar-conta.html',
})
export class AlterarContaPage {
  loading: any;
  userLogged: any;
  type: String;
  dataUserLogged: any;
  formUpdate: FormGroup;
  public data: any;
  public nomeDoUsuario: any;
  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public navCtrl: NavController,
    public clientService: ClientService) {

      let emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
      this.formUpdate = fb.group({
        email: [null, Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
        name: [null, [Validators.required]]
      })

      this.type = this.navParams.get('type');
      this.dataUserLogged = this.navParams.get('userLogged');
      this.userLogged = this.navParams.data.userLogged
    }
  // Ação que acontece quando a página está prestes a ser exibida
  ionViewWillEnter() {
    this.getUser();
  }
  getUser(){
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
  })
  loader.present();
  this.authService.getUser()
      .then(res => {
          this.data = res;
          this.nomeDoUsuario = this.data.name;
          loader.dismissAll();

      }, err => {
          this.showToast("Tivemos problemas ao comunicar com o servidor! Faça Login novamente!", 5000);
          loader.dismissAll();
      })
      .catch(err => {
          let body = JSON.parse(err._body);
          let message = body.message;
          this.showToast(message, 3000);
          loader.dismissAll();
      });
  }

  updateUser(){
    this.authService.updateUser(this.formUpdate.value)
      .then((result) => {
        this.showToast("Usuario alterado com sucesso!", 5000);
        this.navCtrl.setRoot('ChatPage');
      }, (err) => {
        let body = JSON.parse(err._body);
        let message = body.message;
        this.showToast(message);
        this.loading.dismiss();
      });
  }

  // Método para chamar o Toast
  showToast(message: string, duration?: number) {
    let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        showCloseButton: true,
        closeButtonText: "Ok"
    });
    toast.present();
  }
  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AlterarContaPage');
  }
}
