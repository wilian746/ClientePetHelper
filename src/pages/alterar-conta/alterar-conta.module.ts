import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlterarContaPage } from './alterar-conta';

@NgModule({
  declarations: [
    AlterarContaPage,
  ],
  imports: [
    IonicPageModule.forChild(AlterarContaPage),
  ],
})
export class AlterarContaPageModule {}
