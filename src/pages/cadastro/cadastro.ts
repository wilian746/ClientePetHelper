import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  MenuController,
  ToastController,
  LoadingController,
  AlertController,
  ViewController,
  NavParams
} from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
//import * as moment from 'moment';
import { ClientService } from "../../providers/client/clientService"


@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  loading: any;
  dataUserLogged: any;
  type: String;

  formCadastro: FormGroup;
  myDate: String;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public clienteService : ClientService) {
    menuCtrl.enable(false);

    let emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    
        this.formCadastro = fb.group({
          email: [null, Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
          password: [null, [Validators.required, Validators.minLength(6)]],
          name: [null, [Validators.required]],
          confirmarSenha: [null, [Validators.required, Validators.minLength(6)]],
        })

    this.type = this.navParams.get('type');
    this.dataUserLogged = this.navParams.get('userLogged');
  }

  submit() {
    this.showLoader();

    this.clienteService.saveClient(this.formCadastro.value).then((result) => {
      let data: any = result;
      this.loading.dismiss();
      this.showToast(data.message, 3000);

      this.navCtrl.setRoot('LoginPage');
    }, (err) => {
      let body = JSON.parse(err._body);
      let message = body.message;
      this.showToast(message);
      this.loading.dismiss();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  showToast(message: string, duration?: number) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      showCloseButton: true,
      closeButtonText: "Ok"
    });
    toast.present();
  }
}
