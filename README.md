PET HELPER
=====================

#### SEJA FELIZ POR QUE DEUS É BÃO E É ISSO BESMO GRAÇAS AO BAX

## Api Restful

Certifique-se que você tenha instalado o [NodeJs](https://nodejs.org/en/download/) em sua máquina.

Clone o projeto para sua máquina:
```bash
$ git clone <LINK DO REPOSITÓRIO>
```

Após ter o projeto em sua máquina, execute o comando abaixo no terminal para poder instalar as dependências necessárias:
```bash
$ npm install
```

- Configure a conexão com MongoDB no diretório: `config/database.js`

Para rodar o servidor e testar a api, execute:
```bash
$ npm start
```

#### Rota para Registro de Usuário

- Registro
{POST} /api/v1/auth/register
```bash
name:{
    type: String,
    required: true
},
email: {
    type: String,
    required: true
},
password: {
    type: String,
    required: true,
    min: 6
}
```

#### Rotas para Autenticação e Checagem

- Autenticação
{POST} /api/v1/auth/login
```bash
email: {
    type: String,
    required: true
},
password: {
    type: String,
    required: true,
    min: 6
}
```

- Checar Autenticação
{GET} /api/v1/auth/protected
```bash
HEADER: {
    Authorization: JWT <token>
}
```

#### COMANDOS LINUX
```bash
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 9000
```

#### Comandos Linux
```bash
nohup node server.js &
```

#### VER OS PROCESSOS ATIVOS
```bash
ps -ef
```

#### MATAR UM PROCESSO
```bash
kill -9 id_number
```